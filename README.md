# 01 Smart Freshener

<img src="/uploads/add6b85b680697a6bdf28c507165b896/freshener.png"  width="640" height="360">

The Smart Freshener is built to freshen the air automatically. Our system uses Arduino with a variety of sensors connected to recognize different scenarios, and respond appropriately. The following scenarios can be recognized:
- A 'number one' and 'number two' uses
- The toilet is getting cleaned
- The toilet is not in use
- The use is uknown

The software has the following qualities:
- Very modular
- Readable
- Greate use of the language features (C++)
- Debouncing of the buttons
- Use of EEPROm and interrupts
- Not a single delay

See the [report](https://git.science.uu.nl/t.i.bakhtali/interactietech/-/blob/d9914942f2e131a7953eee1e06418149e045862b/01%20Smart%20Freshener/Report.pdf) for more details.

# 02 IOT Water System For Plants

<img src="/uploads/d9e77aa186e524b54293fdebf3ec6483/plant.png" width="600" height="320"> 
<img src="/uploads/caa0cc622ffb8992127adccbc794178b/mobile_interface.png" width="600" height="320">

We built a IOT system to water a plant automatically or manually through a remote interface. We use WiFi to connect to the internet and the MQTT protocol for messaging.

There are three systems in place:
- PubSubClient, so our thing can publish/subscribe to our MQTT broker
- Node-RED, for flow-based control and visualization
- MQTT Mobile app

The software has the following qualities:
- Very modular
- Readable
- Greate use of the language features (C++)

See the [report](https://git.science.uu.nl/t.i.bakhtali/interactietech/-/blob/83e7a81a0a2f6714cba13a327f2e524ca067be28/02%20IOT%20Water%20System%20for%20Plants/Report/Report.pdf) for more details.
