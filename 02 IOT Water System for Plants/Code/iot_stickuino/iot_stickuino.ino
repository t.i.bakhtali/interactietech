#include "precomp.h"

unsigned long current_millis;
MPU* mpu;

void setup() {
  Serial.begin(9600);

  mpu = new MPU(100);
}

void loop() {
  current_millis = millis();
  mpu->update(current_millis);
}
