#include "precomp.h"

MPU::MPU(unsigned int interval) {
  m_interval = interval;

  Wire.begin();
  m_mpu = new MPU6050();
  m_mpu->initialize();
  Serial.println(m_mpu->testConnection() ? "MPU6050 connection successful" : "MPU6050 connection failed");
}

void MPU::update(unsigned long current_millis) {
  if (current_millis - m_previous_millis >= m_interval) {
    m_previous_millis = current_millis;

    m_mpu->getMotion6(&m_acc.x, &m_acc.y, &m_acc.z, &m_gyro.x, &m_gyro.y, &m_gyro.z);
    print();
  }
}

void MPU::print() {
  Serial.print("a/g:\t");
  Serial.print(m_acc.x); Serial.print("\t");
  Serial.print(m_acc.y); Serial.print("\t");
  Serial.print(m_acc.z); Serial.print("\t");
  Serial.print(m_gyro.x); Serial.print("\t");
  Serial.print(m_gyro.y); Serial.print("\t");
  Serial.println(m_gyro.z);
}

VEC3 MPU::getAcceleration() {
  return m_acc;
}

VEC3 MPU::getGyro() {
  return m_gyro;
}
