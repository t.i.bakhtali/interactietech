#ifndef MPU_H
#define MPU_H

struct VEC3 {
  VEC3(){
    x = y = z = 0;
  }
  VEC3(int _x, int _y, int _z){
    x = _x;
    y = _y;
    z = _z;
  }
  int x, y, z;
};

class MPU {
  public:
    MPU(unsigned int interval);
    void update(unsigned long current_millis);
    void print();
    VEC3 getAcceleration();
    VEC3 getGyro();

  private:
    unsigned long m_previous_millis;
    unsigned int m_interval;
  
    MPU6050* m_mpu;
    VEC3 m_acc;
    VEC3 m_gyro;
};

#endif
