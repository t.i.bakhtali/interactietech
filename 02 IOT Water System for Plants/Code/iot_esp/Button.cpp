#include "precomp.h"

Button::Button(byte pin, byte mode) {
  m_pin = pin;
  pinMode(m_pin, mode);
}

void Button::update(unsigned long current_millis) {
  byte reading = digitalRead(m_pin);

  if (reading != m_lastReading) m_previousMillis = current_millis;

  resetStates();

  if (current_millis - m_previousMillis >= DEBOUNCE_DELAY) {
    if (reading != m_currentState) {
      m_currentState = reading;
      updateStates();
      m_lastState = m_currentState;
    }
  }

  m_lastReading = reading;
}

void Button::resetStates() {
  m_pressed = m_released = false;
}

void Button::updateStates() {
  if (m_lastState == NOT_PRESSED && m_currentState == IS_PRESSED) {
    m_pressed = true;
  }
  if (m_lastState == IS_PRESSED && m_currentState == NOT_PRESSED) {
    m_released = true;
  }
}

bool Button::isPressed() {
  return m_pressed;
}

bool Button::isReleased() {
  return m_released;
}
