#ifndef PRECOMP_H
#define PRECOMP_H

#include <avr/pgmspace.h>
#include <Servo.h>
#include <Wire.h>
#include <TimeLib.h> // https://github.com/PaulStoffregen/Time

#include <ESP8266WiFi.h>
#include <PubSubClient.h>

#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#include "AMUX.h"
#include "BME.h"
#include "Button.h"
#include "MicroServo.h"

#endif
