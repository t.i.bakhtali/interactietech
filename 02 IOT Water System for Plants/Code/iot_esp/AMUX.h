#ifndef AMUX_H
#define AMUX_H

class AMUX {
  public:
    AMUX(byte analogIn, byte selectPin, unsigned int interval);
    void update(unsigned long current_millis);
    void updateNow();
    unsigned int getMoisture();
    unsigned int getLight();

  private:
    byte m_analogIn;
    byte m_selectPin;
    unsigned long m_previousMillis;
    unsigned int m_interval;
    float m_light;
    float m_moist;
};

#endif
