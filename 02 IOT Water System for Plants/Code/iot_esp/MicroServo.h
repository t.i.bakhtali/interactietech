#ifndef MSERVO_H
#define MSERVO_H

#define SWEEP_DELAY   15
#define FORWARD_POS   0
#define BACKWARD_POS  180

class MicroServo {
  public:
    MicroServo(byte pin);
    void update(unsigned long current_millis);
    void toggle();
    bool sweepForward();
    bool sweepBackward();

  private:
    unsigned long m_previousMillis;
    bool m_sweepUp = false;

    byte m_pin;
    byte m_pos = 0;
    Servo* m_servo;
};

#endif
