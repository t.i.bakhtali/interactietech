/*
__/\\\\\\\\\\\_______/\\\\\_______/\\\\\\\\\\\\\\\_        
__\/////\\\///______/\\\///\\\____\///////\\\/////__       
_______\/\\\_______/\\\/__\///\\\________\/\\\_______      
________\/\\\______/\\\______\//\\\_______\/\\\_______     
_________\/\\\_____\/\\\_______\/\\\_______\/\\\_______    
__________\/\\\_____\//\\\______/\\\________\/\\\_______   
___________\/\\\______\///\\\__/\\\__________\/\\\_______  
_________/\\\\\\\\\\\____\///\\\\\/___________\/\\\_______ 
_________\///////////_______\/////_____________\///________
____________________________________________________________
________________________________Tariq Bakhtali_5631394_______
___________________________________Rowen Horbach_6572626______
_______________________________________________________________
*/
#include "precomp.h"

#define SCL                 D1
#define SDA                 D2

#define FLASH_BUTTON        D3
#define TEST_BUTTON         D4

#define OLED_RESET          D7

#define SELECT_PIN          D5
#define ANALOG_IN           A0
#define SERVO_PIN           D6

#define MODE_LED            D0
#define AUTOMATIC_MODE      0
#define MANUAL_MODE         1
bool current_mode           = AUTOMATIC_MODE;

const char* WIFI_SSID       = "";
const char* WIFI_PASSWORD   = "";

const char* SERVER          = "mqtt.uu.nl";
const char* USERNAME        = "";
const char* PASSWORD        = "";
WiFiClient wifiClient;
PubSubClient mqttBroker(wifiClient);

AMUX*                 amux;
Adafruit_SSD1306*     oled;
BME*                  bme;
MicroServo*           servo;
Button*               flashButton;
Button*               testButton;

unsigned long         current_millis = 0;
bool                  isWatering = false;
bool                  waterNow = false;
time_t                last_wateringTime = 0;
time_t                elapsed_wateringTime = 0;

void setup() {
  Serial.begin(9600);
  pinMode(MODE_LED, OUTPUT);

  Wire.begin(SDA, SCL);
  bme = new BME(5000);
  init_oled();
  servo = new MicroServo(SERVO_PIN);
  amux = new AMUX(ANALOG_IN, SELECT_PIN, 5000);
  flashButton = new Button(FLASH_BUTTON, INPUT_PULLUP);
  testButton = new Button(TEST_BUTTON);

  connection_init();
}

void loop() {
  current_millis = millis();

  bme->update(current_millis);
  servo->update(current_millis);
  amux->update(current_millis);
  flashButton->update(current_millis);
  testButton->update(current_millis);
  
  connection_update();

  switchMode();
  switch (current_mode) {
    case AUTOMATIC_MODE:
      digitalWrite(MODE_LED, HIGH);
      automaticMode();
      break;
    case MANUAL_MODE:
      digitalWrite(MODE_LED, LOW);
      manualMode();
      break;
  }

  if (!isWatering) displaySensorValues();
  else displayWatering();
  
}

void init_oled() {
  oled = new Adafruit_SSD1306(128, 64, &Wire, OLED_RESET);
  if (!oled->begin(SSD1306_SWITCHCAPVCC, 0x3C)) {
    Serial.println(F("SSD1306 allocation failed"));
  }
  oled->clearDisplay();
}
