#ifndef BUTTON_H
#define BUTTON_H

#define DEBOUNCE_DELAY  50
#define IS_PRESSED      0
#define NOT_PRESSED     1

class Button {
  public:
    Button(byte pin, byte mode = INPUT);
    void update(unsigned long current_millis);
    bool isPressed();
    bool isReleased();
    
  private:
    void resetStates();
    void updateStates();
  
    byte m_pin;
    unsigned long m_previousMillis;
    byte m_lastReading;
    byte m_currentState;
    byte m_lastState;

    bool m_pressed, m_released;
    byte m_counter = 0;
  
};

#endif
