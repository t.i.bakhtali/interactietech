#ifndef BME_H
#define BME_H

#define SEALEVELPRESSURE_HPA (1013.25)

class BME {
  public:
    BME(unsigned int interval);
    void update(unsigned long current_millis);
    void updateNow();
    
    float getTemperature();
    float getPressure();
    float getAltitude();
    float getHumidity();

  private:
    unsigned long m_previousMillis;
    unsigned int m_interval;
  
    Adafruit_BME280* m_bme; 
    float m_temperature;
    float m_pressure;
    float m_altitude;
    float m_humidity;

};

#endif
