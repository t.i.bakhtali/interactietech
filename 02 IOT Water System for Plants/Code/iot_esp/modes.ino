
unsigned long       watering_updateMillis;
const unsigned int  watering_updateInterval = 500;
const unsigned int  desiredMoistLevel = 5; // percentage
unsigned int        watering_timeInterval = 0;
unsigned long       watering_timeMillis;

void switchMode() {
  if (flashButton->isPressed()) {
    current_mode = !current_mode;
    publish_mode();
  }
}

void automaticMode() {
  if (amux->getMoisture() < desiredMoistLevel && !isWatering) {
    isWatering = true;
    watering_updateMillis = current_millis;
    watering_timeMillis = current_millis;
    watering_timeInterval = calculateTime_temperature() + calculateTime_light() + calculateTime_humidity();
    last_wateringTime = now();
    publish_status();
    Serial.print("watering time: "); Serial.println(watering_timeInterval);
  }
  else {
    servo->sweepBackward();
  }

  if (current_millis - watering_timeMillis >= watering_timeInterval && isWatering) {
    isWatering = false;
    watering_timeInterval = 0;
    publish_status();
  }

  if (!isWatering) return;

  servo->sweepForward();

  if (current_millis - watering_updateMillis >= watering_updateInterval) {
    watering_updateMillis = current_millis;
    amux->updateNow();
  }
}

void manualMode() {
  if (testButton->isPressed()) {
    waterNow = !waterNow;
    if (waterNow)current_mode = MANUAL_MODE;
  }

  if (waterNow && !isWatering) {
    isWatering = true;
    watering_updateMillis = current_millis;
    last_wateringTime = now();
    publish_status();
  }
  else if (!waterNow) {
    if (isWatering) {
      isWatering = false;
      publish_status();
    }
    servo->sweepBackward();
  }

  if (!isWatering) return;

  servo->sweepForward();

  if (current_millis - watering_updateMillis >= watering_updateInterval) {
    watering_updateMillis = current_millis;
    amux->updateNow();
  }
}

int calculateTime_temperature() {
  return map(bme->getTemperature(), 10, 30, 0, 3000);
}

int calculateTime_light() {
  return map(amux->getLight(), 0, 100, 0, 1000);
}

int calculateTime_humidity() {
  return map(bme->getHumidity(), 0, 100, 0, -1000);
}
