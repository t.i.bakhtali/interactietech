#include "precomp.h"

BME::BME(unsigned int interval) {
  m_interval = interval;
  unsigned status;
  m_bme = new Adafruit_BME280();
  status = m_bme->begin();
  if (!status) {
    Serial.println("Could not find a valid BME280 sensor, check wiring, address, sensor ID!");
    Serial.print("SensorID was: 0x"); Serial.println(m_bme->sensorID(), 16);
    Serial.print("        ID of 0xFF probably means a bad address, a BMP 180 or BMP 085\n");
    Serial.print("   ID of 0x56-0x58 represents a BMP 280,\n");
    Serial.print("        ID of 0x60 represents a BME 280.\n");
    Serial.print("        ID of 0x61 represents a BME 680.\n");
  }
}

void BME::update(unsigned long current_millis) {
  if (current_millis - m_previousMillis >= m_interval) {
    m_previousMillis = current_millis;

    updateNow();
  }
}

void BME::updateNow() {
  m_temperature = m_bme->readTemperature();
  m_pressure = m_bme->readPressure() / 100.0F; //hPa
  m_altitude = m_bme->readAltitude(SEALEVELPRESSURE_HPA);
  m_humidity = m_bme->readHumidity();
}

float BME::getTemperature() {
  return m_temperature;
}
float BME::getPressure() {
  return m_pressure;
}
float BME::getAltitude() {
  return m_altitude;
}
float BME::getHumidity() {
  return m_humidity;
}
