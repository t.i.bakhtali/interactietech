const PROGMEM char* s_temperature = " Temperature ";
const PROGMEM char* s_celcius     = " C";
const PROGMEM char* s_pressure    = " Pressure    ";
const PROGMEM char* s_pascal      = " hPa";
const PROGMEM char* s_altitude    = " Altitude    ";
const PROGMEM char* s_meter       = " m";
const PROGMEM char* s_humidity    = " Humidity    ";
const PROGMEM char* s_percentage  = " %";
const PROGMEM char* s_moisture    = " Moisture    ";
const PROGMEM char* s_light       = " Brightness  ";

const PROGMEM char* s_watering    = " Watering ";
const PROGMEM char* s_timeElapsed = " Time elapsed ";

byte          display_index = 0;
unsigned long display_millis = 0;
unsigned int  display_interval = 1800;

unsigned long display_wateringMillis = 0;
unsigned int  display_wateringInterval = 800;
bool          display_wateringToggle = false;

void displaySensorValues() {
  if (current_millis - display_millis >= display_interval) {
    display_millis = current_millis;
    display_index++;
    oled->clearDisplay();
    oled->setCursor(0, 0);
  }

  switch (display_index) {
    case 0:
      display_sensorValue(s_temperature, bme->getTemperature(), s_celcius);
      display_sensorValue(s_pressure, bme->getPressure(), s_pascal);
      break;
    case 1:
      display_sensorValue(s_altitude, bme->getAltitude(), s_meter);
      display_sensorValue(s_humidity, bme->getHumidity(), s_percentage);
      break;
    case 2:
      display_sensorValue(s_moisture, amux->getMoisture(), s_percentage);
      display_sensorValue(s_light, amux->getLight(), s_percentage);
      break;
    case 3:
      display_timeElapsed();
      break;
    default:
      display_index = 0;
  }
}

void display_sensorValue(const char* name, float value, const char* unit) {
  oled->setTextSize(1);
  oled->setTextColor(SSD1306_BLACK, SSD1306_WHITE);
  oled->println(name);
  oled->println();
  oled->setTextSize(2);
  oled->setTextColor(SSD1306_WHITE);
  oled->print(value);
  oled->setTextSize(1.5);
  oled->println(unit);
  oled->setTextSize(1);
  oled->println();
  oled->display();
}

void display_status(const char* name, const char* status) {
  oled->clearDisplay();
  oled->setCursor(0, 0);
  oled->setTextSize(1);
  oled->setTextColor(SSD1306_BLACK, SSD1306_WHITE);
  oled->println(name);
  oled->println();
  oled->setTextColor(SSD1306_WHITE);
  oled->println(status);
  oled->display();
}

void displayWatering() {
  if (current_millis - display_wateringMillis >= display_wateringInterval) {
    display_wateringMillis = current_millis;
    display_wateringToggle = !display_wateringToggle;
  }

  if (display_wateringToggle) oled->setTextColor(SSD1306_WHITE);
  else oled->setTextColor(SSD1306_BLACK, SSD1306_WHITE);

  oled->clearDisplay();
  oled->setCursor(5, 28);
  oled->setTextSize(2);
  oled->print(s_watering);
  oled->display();
}

void display_timeElapsed() {
  elapsed_wateringTime = now() - last_wateringTime;
  time_t days = day(elapsed_wateringTime) - 1;
  time_t hours = hour(elapsed_wateringTime);
  time_t minutes = minute(elapsed_wateringTime);
  time_t seconds = second(elapsed_wateringTime);

  oled->clearDisplay();
  oled->setCursor(0, 0);
  oled->setTextSize(1);
  oled->setTextColor(SSD1306_BLACK, SSD1306_WHITE);
  oled->println(s_timeElapsed);
  oled->println();

  oled->setTextSize(2);
  oled->setTextColor(SSD1306_WHITE);
  oled->print(days); oled->print(":");
  oled->print(hours); oled->print(":");
  oled->print(minutes); oled->print(":");
  oled->println(seconds);

  oled->setTextSize(1.5);
  oled->println("D : H : M : S");
  oled->display();
}
