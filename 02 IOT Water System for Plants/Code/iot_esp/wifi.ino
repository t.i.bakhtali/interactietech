//https://pubsubclient.knolleary.net/api.html

const char* DEVICE_NAME           = "EspClient";
// topics
const char* TOPIC_COMMANDS        = "infob3it/085/commands";
const char* TOPIC_MODE            = "infob3it/085/mode";
const char* TOPICS[]              = {TOPIC_COMMANDS, TOPIC_MODE};
const byte  TOPICS_LENGTH         = 2;
const char* TOPIC_TEMPERATURE     = "infob3it/085/temperature";
const char* TOPIC_PRESSURE        = "infob3it/085/pressure";
const char* TOPIC_ALTITUDE        = "infob3it/085/altitude";
const char* TOPIC_HUMIDITY        = "infob3it/085/humidity";
const char* TOPIC_MOISTURE        = "infob3it/085/moisture";
const char* TOPIC_LIGHT           = "infob3it/085/light";
const char* TOPIC_ISWATERING      = "infob3it/085/iswatering";
const char* TOPIC_WATERINGTIME    = "infob3it/085/wateringtime";
const char* TOPIC_LOG             = "infob3it/085/log";
// messages
const char MESSAGE_AUTOMATIC      = 'a';
const char MESSAGE_MANUAL         = 'm';
const char MESSAGE_WATERING       = 'w';
const char MESSAGE_IDLE           = 'i';
const char MESSAGE_UPDATE         = 'u';
const char MESSAGE_CONNECTED      = 'c';
const char MESSAGE_DISCONNECTED   = 'd';

unsigned long connection_millis = 0;
unsigned int  connection_interval = 5000;
char strData[8];

void connection_init() {
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  display_status("Connecting to: ", WIFI_SSID);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500); // delay prevents wdt reset
    oled->print(".");
    oled->display();
  }

  display_status("Connecting to: ", SERVER);
  mqttBroker.setServer(SERVER, 1883);
  mqttBroker.setCallback(mqtt_callback);
  while (!mqttBroker.connected()) {
    connectToBroker();
    oled->print(".");
    oled->display();
    delay(500);
  }
}

void connection_update() {
  if (current_millis - connection_millis >= connection_interval) {
    connection_millis = current_millis;
    if (!mqttBroker.connected()) connectToBroker();
    else {
      publish_sensorData();
      publish_status();
    }
  }
  mqttBroker.loop();
}

void publish_sensorData() {
  dtostrf(bme->getTemperature(), 4, 2, strData);
  mqttBroker.publish(TOPIC_TEMPERATURE, strData, true);
  dtostrf(bme->getPressure(), 6, 2, strData);
  mqttBroker.publish(TOPIC_PRESSURE, strData, true);
  dtostrf(bme->getAltitude(), 5, 2, strData);
  mqttBroker.publish(TOPIC_ALTITUDE, strData, true);
  dtostrf(bme->getHumidity(), 4, 2, strData);
  mqttBroker.publish(TOPIC_HUMIDITY, strData, true);
  dtostrf(amux->getMoisture(), 2, 0, strData);
  mqttBroker.publish(TOPIC_MOISTURE, strData, true);
  dtostrf(amux->getLight(), 2, 0, strData);
  mqttBroker.publish(TOPIC_LIGHT, strData, true);
}

void publish_status() {
  publish_mode();
  publish_watering();
}

void publish_mode() {
  if (current_mode)
    mqttBroker.publish(TOPIC_MODE, String(MESSAGE_MANUAL).c_str(), true);
  else
    mqttBroker.publish(TOPIC_MODE, String(MESSAGE_AUTOMATIC).c_str(), true);
}

void publish_watering() {
  if (isWatering)
    mqttBroker.publish(TOPIC_ISWATERING, String(MESSAGE_WATERING).c_str(), true);
  else
    mqttBroker.publish(TOPIC_ISWATERING, String(MESSAGE_IDLE).c_str(), true);

  dtostrf(watering_timeInterval, 4, 0, strData);
  mqttBroker.publish(TOPIC_WATERINGTIME, strData);
}

void connectToBroker() {
  Serial.println("");
  Serial.println("Attempting MQTT connection...");
  if (mqttBroker.connect(DEVICE_NAME, USERNAME, PASSWORD, TOPIC_LOG, 2, 1, String(MESSAGE_DISCONNECTED).c_str())) { // boolean connect (clientID, username, password, willTopic, willQoS, willRetain, willMessage)
    Serial.println("connected");
    mqttBroker.publish(TOPIC_LOG, String(MESSAGE_CONNECTED).c_str(), true);
    publish_status();
    resubscriptions();
  } else {
    Serial.print("failed, rc="); Serial.println(mqttBroker.state());
  }
}

void resubscriptions() {
  for (int i = 0; i < TOPICS_LENGTH; i++) {
    mqttBroker.subscribe(TOPICS[i]);
  }
}

void mqtt_callback(char* topic, byte* payload, unsigned int plength) {
  if (strcmp(topic, TOPIC_COMMANDS) == 0) {
    processCommand(payload, plength);
  }
  else if (strcmp(topic, TOPIC_MODE) == 0) {
    processMode(payload, plength);
  }
}

void processCommand(byte* payload, unsigned int plength) {
  char command = (char)payload[0];
  switch (command) {
    case MESSAGE_WATERING: {
        waterNow = true;
        current_mode = MANUAL_MODE;
        publish_status();
      }
      break;
    case MESSAGE_IDLE: {
        waterNow = false;
        current_mode = MANUAL_MODE;
        publish_status();
      }
      break;
    case MESSAGE_UPDATE: {
        bme->updateNow();
        amux->updateNow();
        publish_sensorData();
      }
      break;
  }
}

void processMode(byte* payload, unsigned int plength) {
  char mode = (char)payload[0];
  switch (mode) {
    case MESSAGE_AUTOMATIC:
      current_mode = AUTOMATIC_MODE;
      waterNow = false;
      break;
    case MESSAGE_MANUAL:
      current_mode = MANUAL_MODE;
      break;
  }
}
