#include "precomp.h"

MicroServo::MicroServo(byte pin) {
  m_pin = pin;
  m_servo = new Servo();
  m_servo->attach(m_pin);
}

void MicroServo::update(unsigned long current_millis) {
  if (current_millis - m_previousMillis >= SWEEP_DELAY) {
    m_previousMillis = current_millis;

    if (m_sweepUp && m_pos > FORWARD_POS)  m_pos--;
    if (!m_sweepUp && m_pos < BACKWARD_POS) m_pos++;
  }
  if (m_pos != 0 || m_pos != 180)
    m_servo->write(m_pos);
}

void MicroServo::toggle() {
  m_sweepUp = !m_sweepUp;
}

bool MicroServo::sweepForward() {
  m_sweepUp = true;
  return m_pos == FORWARD_POS;
}

bool MicroServo::sweepBackward() {
  m_sweepUp = false;
  return m_pos == BACKWARD_POS;
}
