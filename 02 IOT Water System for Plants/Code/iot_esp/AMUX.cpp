#include "precomp.h"

AMUX::AMUX(byte analogIn, byte selectPin, unsigned int interval) {
  m_analogIn = analogIn;
  m_selectPin = selectPin;
  m_interval = interval;
  pinMode(m_analogIn, INPUT);
  pinMode(selectPin, OUTPUT);
  digitalWrite(m_selectPin, LOW);
}

void AMUX::update(unsigned long current_millis) {
  if (current_millis - m_previousMillis >= m_interval) {
    m_previousMillis = current_millis;
    
    updateNow();
  }
}

void AMUX::updateNow() {
  m_light = analogRead(m_analogIn) * 100 / 1023;
  digitalWrite(m_selectPin, HIGH);
  m_moist = analogRead(m_analogIn) * 100 / 1023;
  digitalWrite(m_selectPin, LOW);
}

unsigned int AMUX::getMoisture() {
  return m_moist;
}

unsigned int AMUX::getLight() {
  return m_light;
}
