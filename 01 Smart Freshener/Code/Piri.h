#ifndef PIRI_H
#define PIRI_H

#define STARTUP_TIME 60

class Piri {
  public:
    Piri(byte pin, unsigned int timeDelay);
    void update(unsigned long &currentMillis);
    bool isWarmingUp() {
      return m_isWarmingUp;
    }
    bool motionDetected() {
      return m_motionDetected;
    }

  private:
    bool          m_isWarmingUp = true;
    bool          m_motionDetected = false;
    unsigned long m_previousMillis = 0;
    byte          m_val = 0;
    byte          m_pin;
    unsigned int  m_interval;
};

#endif
