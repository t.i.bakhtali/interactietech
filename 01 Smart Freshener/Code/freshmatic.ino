/* ==================================================================================
     _____                      _     ______             _                           
    / ____|                    | |   |  ____|           | |                          
   | (___  _ __ ___   __ _ _ __| |_  | |__ _ __ ___  ___| |__   ___ _ __   ___ _ __  
    \___ \| '_ ` _ \ / _` | '__| __| |  __| '__/ _ \/ __| '_ \ / _ \ '_ \ / _ \ '__| 
    ____) | | | | | | (_| | |  | |_  | |  | | |  __/\__ \ | | |  __/ | | |  __/ |    
   |_____/|_| |_| |_|\__,_|_|   \__| |_|  |_|  \___||___/_| |_|\___|_| |_|\___|_|    
                                                                                      
   ====================================================  Tariq Bakhtali, 5631394  == 
   ====================================================  Rowen Horbach , 6572626  == 

================================================================================== */


#include "precomp.h"
#include "Sonar.h"

#define MAX_SPRAYS      2400
#define SPRAY_INTERVAL  500

// EEPROM ADDRESSES
#define SPRAY_ADDR        0
#define DELAY_ADDR        sizeof(unsigned int)                                // size of sprays_left
#define SMALL_FLUSH_ADDR  sizeof(unsigned int) + sizeof(byte)                 // size of sprays_left + spray_delay
#define BIG_FLUSH_ADDR    sizeof(unsigned int) + sizeof(byte) + sizeof(byte)  // size of sprays_left + spray_delay + smallFlushDistance

// USE STATES
#define NOT_IN_USE      0
#define USE_UNKNOWN     1 
#define NUMBER_ONE      2
#define NUMBER_TWO      3
#define CLEANING        4

// CONDITIONS
#define MOVEMENT        0
#define FLUSHED         1
#define LIGHT_ON        2
#define DOOR_OPEN       3

// FSM STATES
#define ASLEEP          0
#define AWAKE           1
#define MENU            2

#define ARROW           0
byte arrow[8] = { 0b00000, 0b00000, 0b01110, 0b01110, 0b11111, 0b11111, 0b01110, 0b00100};

const COLOR dummyColor = COLOR(0, 0, 0);
const COLOR sprayColor = COLOR(255, 0, 0);

LiquidCrystal *lcd;
DT            *dt; // Dallas Temperature
LDR           *ldr;
Piri          *piri;
SPMB          *spmb;
Magnet        *magnet;
RGB           *rgb;

unsigned long current_millis = 0;
volatile byte fsm_state = ASLEEP;
bool          start_menu_timer = false;

unsigned int  sprays_left;
unsigned int  sprays_left_color;
volatile bool spray_once = false;
bool          spray_twice = false;
bool          sprayed_once = false, sprayed_twice = false;
byte          spray_type;
bool          start_spray_timer = false;
byte          spray_delay = 0;
byte          current_delay = 0;
unsigned long spray_millis;
byte          flush_type;
volatile bool triggered = false;

byte          smallFlushDistance;
byte          bigFlushDistance;

const byte    sprayButtonPin = 3; // Interrupt pin
const byte    motionPin = 2;      // Interrupt pin
const byte    airwickPin = 7;

#define DEBUG

#ifdef DEBUG
unsigned long debug_millis = 0;
unsigned int  debug_interval = 600;
#endif

void setup() {
  Serial.begin(9600);

  EEPROM_readAnything(SPRAY_ADDR, sprays_left);
  EEPROM_readAnything(DELAY_ADDR, spray_delay);
  EEPROM_readAnything(SMALL_FLUSH_ADDR, smallFlushDistance);
  EEPROM_readAnything(BIG_FLUSH_ADDR, bigFlushDistance);
  sprays_left_color = sprays_left * 0.01 * 255 / 24;

  pinMode(sprayButtonPin, INPUT);
  pinMode(airwickPin, OUTPUT);
  attachInterrupt(digitalPinToInterrupt(sprayButtonPin), isr_sprayNow, RISING);
  attachInterrupt(digitalPinToInterrupt(motionPin), isr_wakeUp, RISING);

  lcd = new LiquidCrystal(12, 13, A0, A1, A2, A3);
  dt = new DT(4, 2000); // Dallas Temperature
  ldr = new LDR(A4, 500);
  piri = new Piri(2, 500);
  spmb = new SPMB(A5);
  //sonar(11, 10, 256) -> Sonar.h, needs echo (snd parameter) to be PWM
  magnet = new Magnet(8);
  rgb = new RGB(5, 6, 9); // Needs 3 PWM pins

  lcd->begin(16, 2);
  lcd->createChar(ARROW, arrow);
  ldr->init();
}

void loop() {
  checkSprayFlag();
  current_millis = millis();

  dt->update(current_millis);
  ldr->update(current_millis);
  piri->update(current_millis);
  spmb->update(current_millis);
  sonar_update(current_millis);
  magnet->update(current_millis);
  rgb->update(current_millis);

  switch (fsm_state) {
    case ASLEEP:
      rgb->setColor(rgb->lerp(dummyColor, getStateColor(), (float)sprays_left / (float)MAX_SPRAYS));
      displayTemperature();
      if (spmb->getButtonPressed() == BUTTON_ONE) goToMenu();
      break;
    case AWAKE:
      rgb->setColor(rgb->lerp(dummyColor, getStateColor(), (float)sprays_left / (float)MAX_SPRAYS));
      displayTemperature();
      if (spmb->getButtonPressed() == BUTTON_ONE) goToMenu();
      updateConditions();
      if (sleepCondition()) handleSleep();
      break;
    case MENU:
      rgb->setColor(0, 0, 255);
      rgb->stayOn();
      menuState();
      break;
  }
  if (triggered)
  {
    rgb->setColor(sprayColor);
    rgb->stayOn();
    if(current_millis - spray_millis >= spray_delay * 1000) spray();
  }

#ifdef DEBUG
  if (current_millis - debug_millis >= debug_interval) {
    debug_millis = current_millis;
    // debug code
  }
#endif

}

void isr_wakeUp() {
  handleAwake();
}

void displayTemperature() {
  lcd->setCursor(0, 0);
  lcd->print(F("Temperature: "));
  lcd->setCursor(0, 1);
  lcd->print(dt->getTemperature());
}

void goToMenu() {
  lcd->clear();
  fsm_state = MENU;
  start_menu_timer = true;
}

void trigger(byte type) {
  spray_millis = current_millis;
  triggered = true;
  spray_type = type;
}

void isr_sprayNow() {
  trigger(NUMBER_ONE);
}

void spray() {
  triggered = false;
  if (spray_type == NUMBER_ONE) spray_once = true;
  if (spray_type == NUMBER_TWO) spray_twice = true;
}

void checkSprayFlag() {
  if (spray_twice) {
    sprayTwice();
  }
  else if (spray_once) {
    sprayOnce();
  }
}

void sprayTwice() {
  if (!sprayed_twice) {
    digitalWrite(airwickPin, HIGH);
    sprayed_twice = true;
    spray_millis = current_millis;
    sprays_left--;
  }
  else if (sprayed_twice && current_millis - spray_millis >= SPRAY_INTERVAL) {
    digitalWrite(airwickPin, LOW);
    spray_once = true;
    spray_twice = false;
    spray_millis = current_millis;
  }
}

void sprayOnce() {
  if (!sprayed_once && !sprayed_twice || sprayed_twice && current_millis - spray_millis >= SPRAY_INTERVAL) {
    digitalWrite(airwickPin, HIGH);
    sprayed_once = true;
    sprayed_twice = false;
    spray_millis = current_millis;
    sprays_left--;
  }
  else if (sprayed_once && current_millis - spray_millis >= SPRAY_INTERVAL) {
    digitalWrite(airwickPin, LOW);
    spray_once = false;
    sprayed_once = false;

    EEPROM_writeAnything(SPRAY_ADDR, sprays_left);
    sprays_left_color = sprays_left * 0.01 * 255 / 24;
  }
}
