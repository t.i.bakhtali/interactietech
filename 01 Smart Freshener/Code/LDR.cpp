#include "precomp.h"

LDR::LDR(byte pin, unsigned int interval) {
  m_pin = pin;
  m_interval = interval;
  pinMode(m_pin, INPUT);
}

void LDR::init() {
  m_previousLight = analogRead(m_pin);
}

void LDR::update(unsigned long &currentMillis) {
  if (currentMillis - m_previousMillis >= m_interval) {
    m_previousMillis = currentMillis;

    m_currentLight = analogRead(m_pin);

    if (m_currentLight - m_previousLight > 200 || m_currentLight >= MIN_LIGHT) m_lightIsOn = true;
    else if (m_currentLight - m_previousLight < -200) m_lightIsOn = false;

    m_previousLight = m_currentLight;
  }
}
