#include "precomp.h"

Piri::Piri(byte pin, unsigned int timeDelay) {
  m_pin = pin;
  m_interval = timeDelay;
  pinMode(m_pin, INPUT);
}

void Piri::update(unsigned long &currentMillis) {
  if (currentMillis < STARTUP_TIME) return;
  else m_isWarmingUp = false;

  if (currentMillis - m_previousMillis >= m_interval) {
    m_previousMillis = currentMillis;

    m_val = digitalRead(m_pin);
    if (m_val == HIGH) {
      m_motionDetected = true;
    } else {
      m_motionDetected = false;
    }
  }
}
