NewPing sonar(11, 10, 256); // (TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE);

unsigned long sonar_millis = 0;
unsigned int  sonar_interval = 200;
unsigned int  sonar_distance = 1000;

void echoCheck() {
  if (sonar.check_timer()) {
    sonar_distance = sonar.ping_result / US_ROUNDTRIP_CM;
  }
}

void sonar_update(unsigned long currentMillis) {
  if (currentMillis - sonar_millis >= sonar_interval) {
    sonar_millis = currentMillis;
    sonar.ping_timer(echoCheck);
  }
}
