// MENU STATES
#define CALIBRATE_SONAR         0
#define CALIBRATE_SMALL_FLUSH   1
#define CALIBRATE_BIG_FLUSH     2
#define SPRAY_DELAY             3
#define SET_SPRAY_DELAY         4
#define RESET_SPRAYS            5
#define SHOW_SPRAYS_LEFT        6
#define EXIT                    7

byte          menu_state = 0;
bool          next_menu_state = false;
const int     menu_interval = 3000;
unsigned long menu_millis = 0;

const int     message_interval = 2000;
bool          start_message_timer = false;
unsigned long message_millis = 0;
bool          show_message = false;

byte          delay_start_pos = 1;
byte          delay_cursor_pos = 1;
byte          delay_index = 0;
const byte    delay_spacing = 4;
const byte    delay_values[4] = {0, 5, 10, 15};

void menuState() {
  if (start_menu_timer) {
    start_menu_timer = false;
    menu_millis = current_millis;
  }

  byte buttonPressed = spmb->getButtonPressed();
  if (buttonPressed == BUTTON_ONE || buttonPressed == BUTTON_TWO) {
    menu_millis = current_millis;
  }

  if (current_millis - menu_millis >= menu_interval) {
    resetMenu();
    fsm_state = ASLEEP;
    return;
  }

  if (start_message_timer) {
    start_message_timer = false;
    message_millis = current_millis;
    show_message = true;
    lcd->clear();
  }
  if (show_message) {
    if (current_millis - message_millis >= message_interval) {
      show_message = false;
      next_menu_state = true;
    }
  }

  switch (menu_state) {
    case CALIBRATE_SONAR:
      calibrateSonar();
      break;
    case CALIBRATE_SMALL_FLUSH:
      calibrateSmallFlush();
      break;
    case CALIBRATE_BIG_FLUSH:
      calibrateBigFlush();
      break;
    case SPRAY_DELAY:
      sprayDelay();
      break;
    case SET_SPRAY_DELAY:
      setSprayDelay();
      break;
    case RESET_SPRAYS:
      resetSprays();
      break;
    case SHOW_SPRAYS_LEFT:
      showSpraysLeft();
      break;
    case EXIT:
    default:
      menu_state = 0;
      fsm_state = AWAKE;
  }
}

void resetMenu() {
  lcd->clear();
  menu_state = 0;
  next_menu_state = false;
  start_menu_timer = false;
  start_message_timer = false;
  show_message = false;
}

void displayMenuNavigation() {
  lcd->setCursor(0, 1);
  lcd->print(F("YES"));
  lcd->setCursor(12, 1);
  lcd->print(F("NEXT"));
}

void nextMenuState(byte nextState, bool enablePress = true) {
  if (next_menu_state || spmb->getButtonPressed() == BUTTON_TWO && enablePress) {
    lcd->clear();
    menu_state = nextState;
    show_message = false;
    next_menu_state = false;
  }
}

void calibrateSonar() {
  if (!show_message) {
    lcd->setCursor(0, 0);
    lcd->print(F("Calibrate sonar?"));
    displayMenuNavigation();
  }
  if (spmb->getButtonPressed() == BUTTON_ONE) {
    lcd->clear();
    menu_state = CALIBRATE_SMALL_FLUSH;
  }
  nextMenuState(SPRAY_DELAY);
}

void calibrateSmallFlush() {
  if (!show_message) {
    lcd->setCursor(0, 0);
    lcd->print(F("Small flush:"));
    lcd->setCursor(0, 1);
    lcd->print(sonar_distance);
    lcd->print(F("cm"));

    menu_millis = current_millis;
    nextMenuState(CALIBRATE_BIG_FLUSH);

    if (spmb->getButtonPressed() == BUTTON_ONE) {
      smallFlushDistance = sonar_distance;
      EEPROM_writeAnything(SMALL_FLUSH_ADDR, smallFlushDistance);
      start_message_timer = true;
    }
  }
  else {
    lcd->setCursor(0, 0);
    lcd->print(F("Distance set to:"));
    lcd->setCursor(0, 1);
    lcd->print(smallFlushDistance);
    lcd->print(F("cm"));
    nextMenuState(CALIBRATE_BIG_FLUSH);
  }
}

void calibrateBigFlush() {
  if (!show_message) {
    lcd->setCursor(0, 0);
    lcd->print(F("Big flush:"));
    lcd->setCursor(0, 1);
    lcd->print(sonar_distance);
    lcd->print(F("cm"));

    menu_millis = current_millis;
    nextMenuState(CALIBRATE_SONAR);

    if (spmb->getButtonPressed() == BUTTON_ONE) {
      bigFlushDistance = sonar_distance;
      EEPROM_writeAnything(BIG_FLUSH_ADDR, bigFlushDistance);
      start_message_timer = true;
    }
  }
  else {
    lcd->setCursor(0, 0);
    lcd->print(F("Distance set to:"));
    lcd->setCursor(0, 1);
    lcd->print(bigFlushDistance);
    lcd->print(F("cm"));
    nextMenuState(CALIBRATE_SONAR);
  }
}

void sprayDelay() {
  lcd->setCursor(0, 0);
  lcd->print(F("Set spray delay?"));
  displayMenuNavigation();
  if (spmb->getButtonPressed() == BUTTON_ONE) {
    next_menu_state = true;
    nextMenuState(SET_SPRAY_DELAY);
  }
  nextMenuState(RESET_SPRAYS);
}

void setSprayDelay() {
  if (!show_message) {

    lcd->setCursor(delay_cursor_pos, 0);
    lcd->write((uint8_t)ARROW);
    if (spmb->getButtonPressed() == BUTTON_TWO) {
      lcd->clear();
      delay_cursor_pos += delay_spacing;
      delay_index++;
      if (delay_cursor_pos > 15) {
        delay_cursor_pos = delay_start_pos;
        delay_index = 0;
      }
    }
    for (int i = 0; i < 4; i++) {
      lcd->setCursor(i * delay_spacing + delay_start_pos, 1);
      lcd->print(delay_values[i]);
    }

    nextMenuState(RESET_SPRAYS, false);

    if (spmb->getButtonPressed() == BUTTON_ONE) {
      spray_delay = delay_values[delay_index];
      EEPROM_writeAnything(DELAY_ADDR, spray_delay);
      start_message_timer = true;
    }
  } else {
    lcd->setCursor(0, 0);
    lcd->print(F("Delay set to:"));
    lcd->setCursor(0, 1);
    lcd->print(spray_delay);
    lcd->print(F("sec"));
    nextMenuState(RESET_SPRAYS);
  }
}

void resetSprays() {
  if (!show_message) {
    lcd->setCursor(0, 0);
    lcd->print(F("New spray can?"));
    displayMenuNavigation();
  }
  else {
    lcd->setCursor(0, 0);
    lcd->print(F("Sprays reset!"));
  }
  if (spmb->getButtonPressed() == BUTTON_ONE) {
    sprays_left = MAX_SPRAYS;
    EEPROM_writeAnything(SPRAY_ADDR, sprays_left);
    start_message_timer = true;
  }
  nextMenuState(SHOW_SPRAYS_LEFT);
}

void showSpraysLeft() {
  lcd->setCursor(0, 0);
  lcd->print(F("Sprays left:"));
  lcd->setCursor(0, 1);
  lcd->print(sprays_left);
  lcd->setCursor(12, 1);
  lcd->print(F("EXIT"));
  nextMenuState(EXIT);
}
