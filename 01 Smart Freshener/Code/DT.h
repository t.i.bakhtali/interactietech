#ifndef DT_H
#define DT_H
// Dallas Temperature

class DT {
  public:
    DT(byte pin, unsigned int interval);
    ~DT();
    void update(unsigned long &currentMillis);
    float getTemperature() {
      return m_temperature;
    }

  private:
    float m_temperature;
    unsigned long m_previousMillis = 0;
    byte m_pin;
    unsigned int m_interval;
    OneWire *m_oneWire;
    DallasTemperature *m_dt;
};

#endif
