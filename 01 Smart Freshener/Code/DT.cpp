#include "precomp.h"

DT::DT(byte pin, unsigned int interval){
  m_pin = pin;
  m_interval = interval;
  m_oneWire = new OneWire(m_pin);
  m_dt = new DallasTemperature(m_oneWire);  
  m_dt->begin();
}

DT::~DT(){
  delete m_oneWire;
  delete m_dt;
}

void DT::update(unsigned long &currentMillis){
  if(currentMillis - m_previousMillis >= m_interval){
    m_previousMillis = currentMillis;
    m_dt->requestTemperatures();
    m_temperature = m_dt->getTempCByIndex(0);
  }
}
