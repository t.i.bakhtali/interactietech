#include "precomp.h"

SPMB::SPMB(byte pin) {
  m_pin = pin;
  pinMode(m_pin, INPUT);
}

void SPMB::update(unsigned long currentMillis) {
  m_analogValue = analogRead(m_pin);
  unsigned int difference = currentMillis - m_previousMillis;

  if (m_analogValue == m_currentState && m_counter > 0) m_counter -= difference;
  if (m_analogValue != m_currentState) m_counter += difference;

  m_previousMillis = currentMillis;

  resetStates();

  if (m_counter >= m_interval) {
    m_counter = 0;
    m_currentState = m_analogValue;

    updateStates();

    m_lastLabel = m_currentLabel;
  }
}

void SPMB::resetStates() {
  m_buttonPressed = NO_BUTTON;
  m_buttonReleased = NO_BUTTON;
}

void SPMB::updateStates() {
  checkLabel();
  pressedButton();
  releasedButton();
}

void SPMB::checkLabel() {
  if (m_analogValue > IS_PRESSED) {
    m_currentLabel = NO_BUTTON;
  }
  else if (m_analogValue < ONE_MAX) {
    m_currentLabel = BUTTON_ONE;
  }
  else if (m_analogValue < TWO_MAX) {
    m_currentLabel = BUTTON_TWO;
  }
}

void SPMB::pressedButton() {
  if (m_currentLabel != NO_BUTTON && m_lastLabel == NO_BUTTON) {
    m_buttonPressed = m_currentLabel;
  }
}

void SPMB::releasedButton() {
  if (m_currentLabel == NO_BUTTON && m_lastLabel != NO_BUTTON) {
    m_buttonReleased = m_lastLabel;
  }
}
