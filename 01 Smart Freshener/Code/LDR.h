#ifndef LDR_H
#define LDR_H

#define MIN_LIGHT 555

class LDR {
  public:
    LDR(byte pin, int unsigned interval);
    void init();
    void update(unsigned long &currentMillis);
    int value() {
      return m_currentLight;
    }
    bool lightIsOn() {
      return m_lightIsOn;
    }

  private:
    bool          m_lightIsOn = false;
    unsigned long m_previousMillis = 0;
    byte          m_pin;
    int           m_interval;
    int           m_currentLight;
    int           m_previousLight;
};

#endif
