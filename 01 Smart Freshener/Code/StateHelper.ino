const byte    conditionCount = 4;
bool          conditions [conditionCount];
//                              NOT_IN_USE          USE_UNKOWN            NUMBER_ONE        NUMBER_TWO       CLEANING
COLOR         stateColors[] = { COLOR(102,255,255), COLOR(225, 102, 255), COLOR(255,200,0), COLOR(0,255,50), COLOR(100, 0, 100) };

volatile unsigned long awake_millis;
unsigned int awake_interval = 3000;
volatile bool goToSleep = false;

bool sleepCondition() {
  return (magnet->triggered() && !piri->motionDetected()) || goToSleep;
}

void handleAwake() {
  fsm_state = AWAKE;
  triggered = false;
  awake_millis = current_millis;
  goToSleep = false;
  detachInterrupt(digitalPinToInterrupt(motionPin));
}

void handleSleep() {
  byte state = getState();
  fsm_state = ASLEEP;
  if (state == NUMBER_ONE || state == NUMBER_TWO) trigger(state);
  for (byte i = 0; i < conditionCount; i++) conditions[i] = false;
  attachInterrupt(digitalPinToInterrupt(motionPin), isr_wakeUp, RISING);
}

void updateConditions() {  
  if(current_millis - awake_millis >= awake_interval){
    awake_millis = current_millis;
    if(!piri->motionDetected()) goToSleep = true;
  }
  if (!conditions[MOVEMENT]) conditions[MOVEMENT] = piri->motionDetected();
  if (!conditions[FLUSHED]) {
    int distance = sonar_distance;
    byte far = max(smallFlushDistance, bigFlushDistance);
    if (distance < far) {
      conditions[FLUSHED] = true;
      conditions[LIGHT_ON] = ldr->lightIsOn();
      conditions[DOOR_OPEN] = !magnet->hasContact();

      byte nearby = min(smallFlushDistance, bigFlushDistance);
      bool isNearby = distance < nearby;
      if ((isNearby && nearby == smallFlushDistance) || (!isNearby && far == smallFlushDistance)) flush_type = NUMBER_ONE;
      else if ((isNearby && nearby == bigFlushDistance) || (!isNearby && far == bigFlushDistance)) flush_type = NUMBER_TWO;
    }
  }
}

byte getState() {
  if (fsm_state == ASLEEP) return NOT_IN_USE;
  else if (conditions[MOVEMENT] && conditions[FLUSHED] && conditions[LIGHT_ON] && conditions[DOOR_OPEN]) return CLEANING;
  else if (conditions[MOVEMENT] && conditions[FLUSHED] && !conditions[DOOR_OPEN]) return flush_type;
  else return USE_UNKNOWN;
}

COLOR getStateColor() {
  return stateColors[getState()];
}
