#ifndef RGB_H
#define RGB_H

#define FADE_STEP 2

struct COLOR {
  COLOR(byte _r = 0, byte _g = 0, byte _b = 0) {
    r = _r;
    g = _g;
    b = _b;
  }
  unsigned int r, g, b;
};

class RGB {
  public:
    RGB(byte redPin, byte greenPin, byte bluePin); // R GND G B
    void update(unsigned long currentMillis);

    void stayOn();
    void stayOff();
    void setColor(byte r, byte g, byte b);
    void setColor(COLOR color);
    COLOR lerp(COLOR c1, COLOR c2, float t);
    void setFadeSpeed(unsigned int interval);
    void setOnDuration(unsigned int interval);
    void setOffDuration(unsigned int interval);

  private:    
    COLOR         m_color;
    unsigned int  m_interval = 10;
    unsigned long m_previousMillis;
    int           m_fadeValue;
    
    bool          m_stayOn = false;
    bool          m_stayOff = false;
    unsigned int  m_onInterval = 1000;
    unsigned int  m_offInterval = 1000;
    unsigned long m_onMillis;
    unsigned long m_offMillis;
    
    int           m_direction = FADE_STEP;
    byte          m_redPin, m_greenPin, m_bluePin;
};

#endif
