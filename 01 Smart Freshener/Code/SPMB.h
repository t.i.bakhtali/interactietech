#ifndef SPMB_H
#define SPMB_H
// Single Pin Multiple Buttons

// we assume we will use two buttons
// here we define the max analog values per button
#define ONE_MAX    300
#define TWO_MAX    1000
#define IS_PRESSED TWO_MAX

#define NO_BUTTON   0
#define BUTTON_ONE  1
#define BUTTON_TWO  2

class SPMB {
  public:
    SPMB(byte pin);
    void update(unsigned long currentMillis);
    byte getButtonPressed() {
      return m_buttonPressed;
    }
    byte getButtonReleased() {
      return m_buttonReleased;
    }

  private:
    void checkLabel();
    void pressedButton();
    void releasedButton();
    void updateStates();
    void resetStates();

    byte          m_buttonPressed;
    byte          m_buttonReleased;
    byte          m_pin;
    const byte    m_interval = 50;
    byte          m_counter = 0;
    unsigned long m_previousMillis;
    int           m_analogValue;
    int           m_currentState = NO_BUTTON;

    byte          m_currentLabel;
    byte          m_lastLabel;
};

#endif
