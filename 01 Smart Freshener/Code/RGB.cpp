#include "precomp.h"


RGB::RGB(byte redPin, byte greenPin, byte bluePin) {
  m_redPin = redPin;
  m_greenPin = greenPin;
  m_bluePin = bluePin;

  pinMode(m_redPin, OUTPUT);
  pinMode(m_greenPin, OUTPUT);
  pinMode(m_bluePin, OUTPUT);
}

void RGB::update(unsigned long currentMillis) {
  if (m_stayOn && currentMillis - m_onMillis >= m_onInterval) {
    m_stayOn = false;
  }
  if (m_stayOff && currentMillis - m_offMillis >= m_offInterval) {
    m_stayOff = false;
  }

  if (currentMillis - m_previousMillis >= m_interval) {
    m_previousMillis = currentMillis;

    if (!m_stayOn && !m_stayOff)
      m_fadeValue += m_direction;

    if (m_fadeValue > 255) {
      m_stayOn = true;
      m_onMillis = currentMillis;
      m_direction = -FADE_STEP;
      m_fadeValue = 255;
    } else if (m_fadeValue < 0) {
      m_stayOff = true;
      m_offMillis = currentMillis;
      m_direction = FADE_STEP;
      m_fadeValue = 0;
    }
  }

  analogWrite(m_redPin, m_fadeValue * m_color.r / 255);
  analogWrite(m_greenPin, m_fadeValue * m_color.g / 255);
  analogWrite(m_bluePin, m_fadeValue * m_color.b / 255);
}

void RGB::stayOn() {
  analogWrite(m_redPin, m_color.r);
  analogWrite(m_greenPin, m_color.g);
  analogWrite(m_bluePin, m_color.b);
}

void RGB::stayOff() {
  analogWrite(m_redPin, 0);
  analogWrite(m_greenPin, 0);
  analogWrite(m_bluePin, 0);
}

void RGB::setColor(byte r, byte g, byte b) {  
  m_color.r = constrain(r, 0, 255);
  m_color.g = constrain(g, 0, 255);
  m_color.b = constrain(b, 0, 255);
}

void RGB::setColor(COLOR color) {
  m_color = color;
}

COLOR RGB::lerp(COLOR c1, COLOR c2, float t)
{
  t = constrain(t, 0.0, 1.0);
  return COLOR(c1.r + (c2.r - c1.r) * t, c1.g + (c2.g - c1.g) * t, c1.b + (c2.b - c1.b) * t);
}

void RGB::setFadeSpeed(unsigned int interval) {
  m_interval = interval;
}

void RGB::setOnDuration(unsigned int interval) {
  m_onInterval = interval;
}

void RGB::setOffDuration(unsigned int interval) {
  m_offInterval = interval;
}
