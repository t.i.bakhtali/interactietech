#ifndef PRECOMP_H
#define PRECOMP_H

#include <Arduino.h>
#include <EEPROM.h>
#include <LiquidCrystal.h>
#include <DallasTemperature.h>
#include <OneWire.h>
#include <NewPing.h>
#include "EEPROMAnything.h"
#include "DT.h"
#include "LDR.h"
#include "Piri.h"
#include "SPMB.h"
#include "Magnet.h"
#include "RGB.h"

#endif
