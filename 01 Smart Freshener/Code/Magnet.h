#ifndef MAGNET_H
#define MAGNET_H

class Magnet {
  public:
    Magnet(byte pin) {
      m_pin = pin;
    }
    void update(unsigned long &currentMillis) {
      m_wasLow = m_isLow;
      m_isLow = digitalRead(m_pin) == LOW;
    }
    bool hasContact() {
      return digitalRead(m_pin) == LOW;
    }
    bool triggered() {
      return !m_wasLow && m_isLow;
    }

  private:
    byte m_pin;
    bool m_wasLow;
    bool m_isLow;
};

#endif
